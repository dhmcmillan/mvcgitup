﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GitUp4.Startup))]
namespace GitUp4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
