namespace GitUp4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileSizeMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DocumentModels", "FileSize", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DocumentModels", "FileSize");
        }
    }
}
