﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace GitUp4.Models
{
    public class DocumentModel
    {
        [Key]
        public int FileID { get; set; }

        public string FileName { get; set; }

        public string FileType { get; set; }

        public DateTime UploadDate { get; set; }

        public string UploaderEmail { get; set; }

        public string FileSize { get; set; }
    }
}