﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GitUp4.Models;
using System.IO;
using Microsoft.AspNet.Identity;
using System.Web.Security;

namespace GitUp4.Controllers
{
    public class DocumentController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Document
        public ActionResult Index()
        {
            return View(db.DocumentModels.ToList());
        }

        // GET: Document/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentModel documentModel = db.DocumentModels.Find(id);
            if (documentModel == null)
            {
                return HttpNotFound();
            }
            return View(documentModel);
        }

        // GET: Document/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Document/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DocumentModel documentModel, HttpPostedFileBase file)
        {
            if (ModelState.IsValid && TypeVal(Path.GetExtension(file.FileName)) == true)
            {
                string path = Path.Combine(HttpContext.Server.MapPath("~/App_Data/Uploads"), Path.GetFileName(file.FileName));
                file.SaveAs(path);

                db.DocumentModels.Add(new DocumentModel
                {
                    FileID = documentModel.FileID,
                    FileName = Path.GetFileNameWithoutExtension(file.FileName),
                    FileType = Path.GetExtension(file.FileName),
                    UploadDate = DateTime.Now,
                    UploaderEmail = User.Identity.GetUserName(),
                    FileSize = BytesToString(file.ContentLength)
                });

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(documentModel);
        }

        // GET: Document/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentModel documentModel = db.DocumentModels.Find(id);
            if (documentModel == null)
            {
                return HttpNotFound();
            }
            return View(documentModel);
        }

        // POST: Document/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FileID,FileName,FileType,UploadDate,UploaderEmail")] DocumentModel documentModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(documentModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(documentModel);
        }

        // GET: Document/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentModel documentModel = db.DocumentModels.Find(id);
            if (documentModel == null)
            {
                return HttpNotFound();
            }
            return View(documentModel);
        }

        // POST: Document/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DocumentModel documentModel = db.DocumentModels.Find(id);
            db.DocumentModels.Remove(documentModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //validates that document being uploaded is correct type of document
        static Boolean TypeVal(string fileType)
        {
            if
                (fileType == ".docx" ||
                fileType == ".doc" ||
                fileType == ".ppt" ||
                fileType == ".pptx" ||
                fileType == ".xlsx" ||
                fileType == ".pdf" ||
                fileType == ".txt" ||
                fileType == ".csv")
                return true;
            else
                return false;
        }

        //Value calculation
        static String BytesToString(long FileSize)
        {
            string[] suf = { "B", "KB", "MB", "GB"};
            if (FileSize == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(FileSize);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(FileSize) * num).ToString() + suf[place];
        }

        //public ActionResult Download(string filePath, string filename)
        //{
            //string fullName = Path.Combine("~/App_Data/Uploads", filePath, filename);

            //byte[] fileBytes = GetFile(fullName);
            //return File(
                //fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        //}

        //Not working currently
        //public FileResult Download(string filename, HttpPostedFileBase file)
        //{
        //byte[] fileBytes = System.IO.File.ReadAllBytes(Path.GetFullPath(file.FileName));
        //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        //}
    }
}
